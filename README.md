# &#x1F40B; Docker-Einführung

<!-- ```{figure} figures/Docker_container_engine_logo.svg
:alt: Docker-Logo
:align: center
``` -->

<p align="center"><img src="figures/Docker_container_engine_logo.svg" /></p>

Zum Einstieg empfiehlt sich die Dokumentation von [Docker](https://docs.docker.com/). Es ist sehr gut geschrieben und erklärt die wichtigsten Konzepte und Befehle. Es ist auch für Einsteiger geeignet, die noch nie mit Docker gearbeitet haben. Vor allem die Kapitelsequenzen in [Get started](https://docs.docker.com/get-started/) lohnen sich selbst für Erfahrene.


## &#x1F914; Was ist das?

Docker wird verwendet um Apps, Software und KI-Modelle zu implementieren. Docker ist eine Software, die es ermöglicht, Container zu erstellen. Container sind eine Art virtuelle Maschine, die auf einem Rechner läuft. Container sind leichter und schneller als virtuelle Maschinen. Docker ist eine Open-Source-Software, die von Docker Inc. entwickelt wird. Docker Inc. ist ein Tochterunternehmen von Microsoft.

Im Gegensatz zu Virtuellen Maschinen, die eine komplette Betriebssystemumgebung enthalten, enthalten Container nur die Anwendungssoftware und alle Abhängigkeiten:

<p align="center"><img src="figures/docker-containerized-and-vm-transparent-bg.webp" /></p>
Quelle: [Docker](https://www.docker.com/resources/what-container/)

## &#x1F6A2; Images und Container

### Image: 
Ausgang eines jeden Docker-Containers und beinhaltet die Anwendungssoftware und alle Abhängigkeiten. Ein Image ist eine Art Vorlage, aus der ein Container erstellt wird. Ein Image kann mehrere Container erstellen.

### Container: 
Ein Container ist eine Instanz eines Images. Ein Container ist ein laufender Prozess. Ein Container kann mehrere Prozesse enthalten. Ein Container kann auch aus einem anderen Container erstellt werden. Ein Container kann auch aus einem Image erstellt werden.

<!-- ### &#x2693; Volumes

```{epigraph}
*Volumes*: Ein Volume ist ein Verzeichnis, das außerhalb des Containers liegt. Vor allem für Updates eines Containers ist es wichtig, da sonst die Daten verloren gehen.
```

### &#x1F4BE; Services, Stacks und Cluster (Swarm)

Führt man Docker nur auf einem Rechner aus, so ist das oben aufgeführte Konzept von Containern ausreichend.

```{epigraph}
*Services*
```

```{epigraph}
*Stacks*
```

```{epigraph}
*Cluster (Swarm)*
``` -->

# &#x2753; Hilfeseiten für Docker

&#x1F4C1; Docker Docs: How to build, share, and run any applications<br>
https://docs.docker.com/

&#x1F433; Command Line Interface (CLI):<br>
https://docs.docker.com/engine/reference/commandline/cli/

&#x1F40D; Python-specific guide:<br>
https://docs.docker.com/language/python/

# &#x1F4BB; Installation

## Docker Desktop für Windows

https://docs.docker.com/desktop/install/windows-install/

Falls das *Windows Subsystem for Linux* (WSL) nicht installiert ist, kann dies mit dem folgenden Befehl installiert werden:

```powershell
wsl --update
```

Nach der erfolgreichen Installation von Docker kann die Version mit dem folgenden Befehl überprüft werden:

```powershell
docker version
```

Docker-Tutorial Windows

```powershell
docker run -d -p 80:80 docker/getting-started
```

und schon hast du ein Docker-Image gestartet. Dies läuft nun auf http://localhost/tutorial/ weiter.



## Docker Desktop für Mac & Linux

https://docs.docker.com/desktop/install/mac-install/

Die Image-Datei einfach in den &#x1F4C1; Ordner *Applications* ziehen. Nach dem ersten Start muss das Passwort eingegeben werden, um benötigte Treiber zu installieren. Docker läuft im Hintergrund und kann über das Dock gestartet werden. Standardmäßig wird Docker automatisch gestartet, wenn der Mac hochgefahren wird. Docker Desktop prüft regelmäßig auf neue Versionen. Man kann im Einstellungsdialog den Speicher (RAM und Festplatte) für Docker festlegen.

Bei Docker Desktop für Mac wird direkt ein Tutorial gestartet, was genau die gleichen Resultate liefert wie oben bei Windows.


```powershell
docker run --name repo alpine/git clone \
https://github.com/docker/getting-started.git
docker cp repo: /git/getting-started/ 
```

```powershell
cd getting-started
docker build -t docker101tutorial 
```

```powershell
docker run -d -p 80:80 \
--name docker-tutorial docker101tutorial
```

Nun kann man den Container starten und auf http://localhost/tutorial/ ein Tutorial sehen.


# &#x1F40D; Beispiele mit Python

## Webserver

Erstelle einen neuen &#x1F4C1; Ordner mit dem Namen `hello-world-python` und erstelle eine Datei mit dem Namen `server.py` mit folgendem Inhalt:

```python
#!/usr/bin/env python3

from http.server import BaseHTTPRequestHandler, HTTPServer
import os, datetime

class myServer(BaseHTTPRequestHandler):
  def do_GET(self):
    load = os.getloadavg()
    html = """<!DOCTYPE html>
<html>
  <head>
    <title>Hello world</title>
    <meta charset="utf-8" />
  </head>
  <body>
    <h1>Hello world: python</h1>
    Serverzeit: {now}<br />
    Serverauslastung (load): {load}
  </body>
</html>""".format(now=datetime.datetime.now().astimezone(), load=load[0])
    self.send_response(200)
    self.send_header('Content-type','text/html')
    self.end_headers()
    self.wfile.write(bytes(html, "utf8"))
    return

def run():
  addr = ('', 8080)
  httpd = HTTPServer(addr, myServer)
  httpd.serve_forever()

run()
```

Erstelle eine Datei mit dem Namen `Dockerfile` mit folgendem Inhalt:

```dockerfile
# Datei: hello-world-python/Dockerfile (docbuc/hello-world-python)
FROM python:3
ENV TZ="Europe/Amsterdam"
COPY server.py /src/
EXPOSE 8080
USER www-data
CMD ["python", "/src/server.py"]
```

Anschließend kann das Image mit dem folgenden Befehl erstellt werden:

```powershell
docker build -t docbuc/hello-world-python .
```

Der `.` am Ende des Befehls ist wichtig, da sonst Docker nicht weiß, wo der *Dockerfile* liegt.


Nun kann auch der Container gestartet werden:

```powershell
docker run -d -P --name hello-world docbuc/hello-world-python
```

Der Parameter `-P` sorgt dafür, dass Docker automatisch einen freien Port auf dem Host-System auswählt und diesen an den Container weiterleitet. Dieser Port kann mit dem folgenden Befehl ermittelt werden:

```powershell
docker port hello-world
```

Der Output sieht dann wie folgt aus:

```powershell
8080/tcp -> 0.0.0.0:32772
```


Nun kann der Server über den Browser aufgerufen werden: http://localhost:8080/

# 🌐 GeoServer einrichten

<!-- ```{figure}
:alt: GeoServer-Logo
:align: center
:width: 75%
``` -->

<p align="center"><img src="figures/1224px-GeoServer_Logo.svg.png" /></p>

[GeoServer](https://geoserver.org/) ist ein Open-Source-Server zum Austausch von Geodaten. Entworfen für die Interoperabilität, veröffentlicht er Daten aus jeder beliebigen Quelle für räumliche Daten mit offenen Standards.

## OGC Implementierung

GeoServer implementiert OGC-Protokolle wie Web Feature Service (WFS), Web Map Service (WMS) und Web Coverage Service (WCS). Zusätzliche Formate und Veröffentlichungsoptionen sind als Erweiterungen verfügbar, darunter Web Processing Service (WPS) und Web Map Tile Service (WMTS).

## Installation und Konfiguration

Nachdem schon Docker installiert wurde, kann GeoServer mit folgendem Befehl installiert werden:

```bash
docker pull docker.osgeo.org/geoserver:2.23.0
```

Anschließend startet man den GeoServer mit folgendem Befehl und bindet den Ordner `C:/geoserver` (**muss vorher existieren!**) an den Ordner `/opt/geoserver_data` im Container. Außerdem wird die Umgebungsvariable `SKIP_DEMO_DATA` auf `true` gesetzt, damit keine Demo-Daten geladen werden. Somit wird der GeoServer ohne Daten gestartet, was auch die Laufzeit des Startvorgangs verkürzt.

```bash
docker run --mount type=bind,src=C:/geoserver,target=/opt/geoserver_data -it -p8080:8080 --env SKIP_DEMO_DATA=true --name gdi_geoserver docker.osgeo.org/geoserver:2.23.0
```

Anschließend startet der Server, was etwas Zeit in Anspruch nehmen kann. Der GeoServer ist dann unter http://localhost:8080/geoserver erreichbar. Somit hast du nun einen Docker-Container erstellt.

```bash
username **admin**
password **geoserver**
```

# 🗺️ OpenLayers - Beispiel

## Geoportal "Grüne" und "blaue" Flächen in Hamburg

Das Geoportal ist auf <a href="https://geoinfo4.bloch-geo.de/wms/" target="_blank">geoinfo4.bloch-geo.de/wms/</a> zu finden.

Dies kann ein Beispiel einer Dokumentation sein.


# 🧑🏼‍💻 WMS in Geoserver

## 📝 WMS einrichten

1. Unter `C:/geoserver/data` einen 📁 Ordner anlegen (bsplw. `buildingsHH`) und in diesem die Shape-Datei mit den dazugehörigen Daten ablegen.
2. <u>Neuen Workspace anlegen</u><br>
   **Name:** `h**1**` (eure h-Kennung bsplw.)<br>
   **URI:** `http://localhost:8080/geoserver/h**1**`<br>
   (kann auch eine andere URI sein, es bietet sich aber an)<br>
   &rarr; **Speichern**
3. <u>Datenspeicher anlegen &rarr; *Directory of spatial files*</u><br>
   **Name der Datenquelle:** `...`<br>
   **Beschreibung:** `...`<br>
   **Verzeichnis:** `...`<br>
   &rarr; **Speichern**
4. <u>Layer anlegen &rarr; *Publizieren*</u><br>
   **CRS** und **Declared** sollten vorher in QGIS überprüft werden<br>
   **Verw.** `erzwinge angegebenes`<br>
   **Begr. Rechteck** &rarr; *aus Daten berechnen* **und** *aus nativen Grenzen berechnen*<br>
   &rarr; **Speichern**
5. <u>Layer-Vorschau</u><br>
   **OpenLayers3** aus der Liste auswählen<br>
   Nun öffnet sich der WMS in einem neuen Tab, anschließend kann der Link kopiert werden und in QGIS als WMS eingebunden werden.


## 🎨 Stylen des WMS

1. <u>SLD-Vorlage verwenden</u><br>
   <a href="https://docs.geoserver.org/stable/en/user/styling/sld/cookbook/index.html">SLD-Cookbook</a> von GeoServer verwenden<br>
   Dort `Polygon with styled label` auswählen und den Code herunterladen
2. <u>Symbolisierung anpassen</u><br>
   **Fill** (löschen, falls keine Füllung gewünscht ist / auch unten)<br>
   **Stroke** (bspw. `#378ADC`)<br>
   **Label** (ggf. Attribut anpassen)<br>
   Datei unter neuem Namen abspeichern (noch nicht in GeoServer hochladen!)
3. <u>Style hinzufügen</u><br>
   **Stildatei hochladen** ... **Hochladen**<br>
   **Name** der Datei wird übernommen &rarr;  **Arbeitsbereich** auswählen<br>
    &rarr; **Validieren** &rarr; **Speichern**
4. <u>Style anwenden</u><br>
   **Layer bearbeiten** &rarr; **Publizierung** (WMS-Einstellungen)<br>
   neuen verfügbaren Style zu gewählte Stile hinzufügen<br>
   Standardstil auf den neuen Stil einstellen<br>
   &rarr; **Speichern**


In QGIS nun den Dienst aktualisieren oder eine **neue** Layer-Vorschau öffnnen, um die Änderungen zu sehen.
